/*
 * The MIT License
 *
 * Copyright 2019 Yassmine BOUDILI École Nationale des Sciences Géographiques.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package yboudili.colloque.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;


/**
 * Participant Model class with Hibernate persistence
 * @author Yassmine BOUDILI
 */
@Entity
@Table(name = "participant")
public class Participant{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "num_participant", nullable = false)
    private Long num_participant;
    
    @Column(name = "last_name", nullable = false)
    @NotEmpty(message = "Required")
    private String last_name;
    
    @Column(name = "first_name", nullable = false)
    @NotEmpty(message = "Required")
    private String first_name;
    
    @Column(name = "email", nullable = false)
    @NotEmpty(message = "Required")
    private String email;
    
    @Column(name = "birth_date", nullable = false)
    @NotEmpty(message = "Required")
    private String birth_date;
    
    @Column(name = "organisation", nullable = false)
    @NotEmpty(message = "Required")
    private String organisation;
    
    @Column(name = "observations", nullable = false)
    private String observations;
    
    @ManyToOne
    @NotNull(message = "You must register to an event!")
    private Event event;

    // ------------- Constructors -------------- //

    public Participant() {}

    public Participant(String last_name, String first_name, String email, String birth_date, String organisation, String observations, Event event) {
        this.last_name = last_name;
        this.first_name = first_name;
        this.email = email;
        this.birth_date = birth_date;
        this.organisation = organisation;
        this.observations = observations;
        this.event = event;
    }
          
    // -------------- To String -------------- //

    @Override
    public String toString() {
        return "Participant{" + "last_name=" + last_name + ", first_name=" + first_name + ", email=" + email + ", birth_date=" + birth_date + ", organisation=" + organisation + ", observations=" + observations + ", event=" + event.getName() + '}';
    }
     
    // --------------- Setters -------------- //
    
    public void setId(Long num_participant) {
		this.num_participant = num_participant;
	}

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

	public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setBirth_date(String birth_date) {
        this.birth_date = birth_date;
    }

    public void setOrganisation(String organisation) {
        this.organisation = organisation;
    }

    public void setObservations(String observations) {
        this.observations = observations;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    // ---------------- Getters ---------------- //

    public Long getNum_participant() {
        return num_participant;
    }
        
    public String getLast_name() {
        return last_name;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getEmail() {
        return email;
    }

    public String getBirth_date() {
        return birth_date;
    }

    public String getOrganisation() {
        return organisation;
    }

    public String getObservations() {
        return observations;
    }

    public Event getEvent() {
        return event;
    }
          
}

