/*
 * The MIT License
 *
 * Copyright 2019 Yassmine BOUDILI École Nationale des Sciences Géographiques.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package yboudili.colloque.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import org.hibernate.annotations.GenericGenerator;

/**
 * Event Model class with Hibernate persistence
 * @author Yassmine BOUDILI
 */
@Entity
@Table(name = "event")
public class Event {
    
    @Id
    @GeneratedValue(generator="increment")
    @GenericGenerator(name="increment", strategy = "increment")
    @Column(name = "num_event", nullable = false)
    private Long num_event;
    
    @Column(name = "name", nullable = false)
    @NotEmpty(message = "Required")
    private String name;
    
    @Column(name = "theme")
    private String theme;
    
    @Column(name = "event_date")
    @NotEmpty(message = "Required")
    private String date;
    
    @Column(name = "event_period")
    @NotEmpty(message = "Required")
    private String period;
    
    @Column(name = "max_part")
    @NotNull(message = "Required")
    private int max_part;
    
    @Column(name = "description")
    private String description;
    
    @Column(name = "organizer")
    @NotEmpty(message = "Required")
    private String organizer;
    
    @Column(name = "event_type")
    private String event_type;
    
    @OneToMany(mappedBy = "event", cascade = CascadeType.REMOVE)
    private List<Participant> participants = new ArrayList<Participant>();
    
    // --------------- Constructors -------------- //

    public Event() {}

    public Event(String name, String theme, String date, String period, int max_part, String description, String organizer, String event_type) {
        this.name = name;
        this.theme = theme;
        this.date = date;
        this.period = period;
        this.max_part = max_part;
        this.description = description;
        this.organizer = organizer;
        this.event_type = event_type;
    }

    // -------------- To String -------------- //

    @Override
    public String toString() {
        return "Event{" + "name=" + name + ", theme=" + theme + ", date=" + date + ", period=" + period + ", max_part=" + max_part + ", description=" + description + ", organizer=" + organizer + ", event_type=" + event_type + '}';
    }

    // --------------- Setters -------------- //
    
    public void setId(Long num_event) {
		this.num_event = num_event;
	}
    
    public void setName(String name) {
        this.name = name;
    }

	public void setTheme(String theme) {
        this.theme = theme;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public void setMax_part(int max_part) {
        this.max_part = max_part;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setOrganizer(String organizer) {
        this.organizer = organizer;
    }

    public void setEvent_type(String event_type) {
        this.event_type = event_type;
    }

    public void setParticipants(List<Participant> participants) {
        this.participants = participants;
    }
    
    // --------------- Getters -------------- //

    public Long getNum_event() {
        return num_event;
    }
    
    public String getName() {
        return name;
    }

    public String getTheme() {
        return theme;
    }

    public String getDate() {
        return date;
    }

    public String getPeriod() {
        return period;
    }

    public int getMax_part() {
        return max_part;
    }

    public String getDescription() {
        return description;
    }

    public String getOrganizer() {
        return organizer;
    }

    public String getEvent_type() {
        return event_type;
    }

    public List<Participant> getParticipants() {
        return participants;
    }
    
}
