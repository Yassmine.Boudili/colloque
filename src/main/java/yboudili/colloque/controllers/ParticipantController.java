/*
 * The MIT License
 *
 * Copyright 2019 Yassmine BOUDILI École Nationale des Sciences Géographiques.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package yboudili.colloque.controllers;
import yboudili.colloque.models.Participant;
import yboudili.colloque.services.EventService;
import yboudili.colloque.services.ParticipantService;

import java.util.List;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * Controller class for Participant Model
 * @author Yassmine BOUDILI
 */
@Controller
public class ParticipantController {
    
    @Autowired
    ParticipantService participantService;
    @Autowired
    EventService eventService;
    
    // returns a page with a table of all participants
    @GetMapping("/participants")
    public String getAllParticipant(Model model) {
        List<Participant> participants = (List<Participant>) participantService.findAll();
        model.addAttribute("participants", participants);
        return "showParticipants";
    }
    
    // returns the form page for adding a new participant
    @GetMapping("/addParticipant")
    public String addParticipant(Model model) {
        model.addAttribute("participant", new Participant());
        model.addAttribute("events", eventService.findAll()); // include event relation
        return "addParticipant"; 
    }
    
    // adds and saves the participant after validating the form data
    @PostMapping("/saveParticipant")
    public String saveParticipant(@Valid Participant participant, BindingResult result, Model model) {
        if (result.hasErrors()) {
        	model.addAttribute("events", eventService.findAll());
            return "addParticipant";
        }
        participantService.save(participant);
        return "redirect:/participants";
    }
    
    // returns the form page for editing an existing participant
    @GetMapping("/editParticipant/{id}")
    public String editParticipant(@PathVariable("id") long id, Model model) {
    	Participant participant = participantService.findById(id)
          .orElseThrow(() -> new IllegalArgumentException("Invalid participant Id:" + id));
    	model.addAttribute("participant", participant);
    	model.addAttribute("events", eventService.findAll());
        return "editParticipant";
    }
    
    // updates and saves the participant after validating the form data
    @PostMapping("/updateParticipant/{id}")
    public String updateParticipant(@PathVariable("id") long id, @Valid Participant participant, BindingResult result, Model model) {
        if (result.hasErrors()) {
            participant.setId(id);
        	model.addAttribute("events", eventService.findAll());
            return "editParticipant";
        }
        participantService.save(participant);
        return "redirect:/participants";
    }
    
    // deletes a participant
    @GetMapping("/deleteParticipant/{id}")
    public String deleteParticipant(@PathVariable("id") long id, Model model) {
        Participant participant = participantService.findById(id)
          .orElseThrow(() -> new IllegalArgumentException("Invalid participant Id:" + id));
        participantService.delete(participant);
        return "redirect:/participants";
    }
    
}
