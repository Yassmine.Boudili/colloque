/*
 * The MIT License
 *
 * Copyright 2019 Yassmine BOUDILI École Nationale des Sciences Géographiques.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package yboudili.colloque.controllers;

import yboudili.colloque.models.Event;
import yboudili.colloque.services.EventService;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * Controller class for Event Model
 * @author Yassmine BOUDILI
 */
@Controller
public class EventController {
	
    @Autowired
    EventService eventService;
    
    // returns a page with a table of all events
    @GetMapping("/events")
    public String getAllEvents(Model model) {
        model.addAttribute("events", eventService.findAll());
        return "showEvents";
    }
    
    // returns the form page for adding a new event
    @GetMapping("/addEvent")
    public String addEvent(Model model) {
        Event event = new Event();
        event.setMax_part(20); // default value for max participants
        model.addAttribute("event", event);
        return "addEvent";
    }
    
    // adds and saves the event after validating the form data
    @PostMapping("/saveEvent")
    public String saveEvent(@Valid Event event, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "addEvent";
        }
        eventService.save(event);
        return "redirect:/events";
    }
    
    // returns the form page for editing an existing event
    @GetMapping("/editEvent/{id}")
    public String editEvent(@PathVariable("id") long id, Model model) {
    	Event event = eventService.findById(id)
          .orElseThrow(() -> new IllegalArgumentException("Invalid event Id:" + id));
    	model.addAttribute("event", event);
        return "editEvent";
    }
    
    // updates and saves the event after validating the form data
    @PostMapping("/updateEvent/{id}")
    public String updateEvent(@PathVariable("id") long id, @Valid Event event, BindingResult result, Model model) {
        if (result.hasErrors()) {
            event.setId(id);
            return "editEvent";
        }
        eventService.save(event);
        return "redirect:/events";
    }
    
    // deletes an participant
    @GetMapping("/deleteEvent/{id}")
    public String deleteEvent(@PathVariable("id") long id, Model model) {
        Event event = eventService.findById(id)
          .orElseThrow(() -> new IllegalArgumentException("Invalid event Id:" + id));
        eventService.delete(event);
        return "redirect:/events";
    }
    
}
