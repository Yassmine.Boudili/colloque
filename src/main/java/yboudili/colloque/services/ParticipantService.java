/*
 * The MIT License
 *
 * Copyright 2019 Yassmine BOUDILI École Nationale des Sciences Géographiques.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package yboudili.colloque.services;

import yboudili.colloque.models.Participant;
import yboudili.colloque.repositories.ParticipantRepo;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service class for CRUD operations on Participant Model
 * @author Yassmine BOUDILI
 */
@Service
public class ParticipantService {
    
    @Autowired
    private ParticipantRepo repository;

    public List<Participant> findAll() {
        List<Participant> participants = (List<Participant>) repository.findAll();
        return participants;
    }
    
    public void save(Participant participant) {
        repository.save(participant);
    }
    
    public Optional<Participant> findById(Long id) {
    	return repository.findById(id);
    }
    
    public void delete(Participant participant) {
    	repository.delete(participant);
    }
    
}
