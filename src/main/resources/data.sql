INSERT INTO 
event (num_event, name, theme, event_date, event_period, max_part, description, organizer, event_type)
VALUES
(1, 'JavaEE training', 'JavaEE', '2019-02-15', '5 days', 19, '', 'ENSG', 'training'),
(2, 'Graduation party', 'Graduation', '2019-03-28', '4 hours', 140, '', 'ENSG', 'party');

INSERT INTO
participant ( num_participant, last_name, first_name, email, birth_date, organisation, observations, event_num_event)
VALUES 
(1, 'Boudili', 'Yassmine', 'yassmine.boudili@jee.org', '1995-05-29', 'ensg', 'none', 2),
(2, 'Amany','Jean','jean.amany@jee.org', '1995-02-04', 'upem', 'none', 1),
(3, 'Ghadhab','Hiba','hiba.ghadhab@jee.org', '1993-05-24', 'upem', 'none', 2),
(4, 'Thaalbi','Sinda','sinda.thaalbib@jee.org', '1994-06-15', 'ensg', 'none', 1),
(5, 'Permalnaick','Max','max.pemal@jee.org', '1986-08-11', 'ensg', 'none', 2);
