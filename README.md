# Mini projet Colloque

Colloque est une application Spring Boot JavaEE qui implémente la persistance des données avec Hibernate.
Ce mini projet s'inscrit dans le programme TSI de l'École Nationale des Sciences Géographiques 2018/2019.

## Prérequis

Vous devez disposer d'une base de données postgresql nommée **colloque_yboudili** accessible par l'utilisateur par défault:  `postgres` .  

## Installation
 1. Cloner le dépôt  
 ``git clone https://gitlab.com/Yassmine.Boudili/colloque ``  
 ``cd colloque``
 2. Installer et lancer l'application  
 ``mvn package``  
 ``cd target/``  
 ``java -jar colloque-1.0-SNAPSHOT.jar``
 3. Ensuite accedez à l'application à travers l'adresse  
 ``http://localhost:8080``

